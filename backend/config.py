#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
"""

hostName = "127.0.0.1"
serverPort = 9101
exporter_prefix = "msv_clubhouse_"

ttn_user = "USER@ttn"
ttn_key = "TTN KEY"
ttn_region = "EU1"	