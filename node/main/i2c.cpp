

#include "i2c.h"

#include "esp_log.h"


void i2c_init()
{
	i2c_config_t i2c_config;

		i2c_config.mode = I2C_MODE_MASTER;
		i2c_config.sda_io_num = I2C_SDA_PIN;
		i2c_config.scl_io_num = I2C_SCL_PIN;
		i2c_config.sda_pullup_en = GPIO_PULLUP_ENABLE;
		i2c_config.scl_pullup_en = GPIO_PULLUP_ENABLE;
		i2c_config.master.clk_speed = I2C_MASTER_FREQ_HZ;


	ESP_ERROR_CHECK(i2c_param_config(I2C_NUM, &i2c_config));
	ESP_ERROR_CHECK(i2c_driver_install(I2C_NUM, I2C_MODE_MASTER, 0, 0, 0));
}