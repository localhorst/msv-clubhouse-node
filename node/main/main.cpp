#include "main.h"

#include "ssd1366.h"

void sendMessages(void* pvParameter)
{
    while (1) {
        dummyPayload();
        printf("Sending msg...\n");
        ssd1306_display_clear();
        const char *pcTask1 = "Sending msg...\n";
	    xTaskCreate(&task_ssd1306_display_text, "task_ssd1306_display_text", 4096, (void *) pcTask1, 5, NULL);
        TTNResponseCode res = ttn.transmitMessage((uint8_t*) &pDummyPayload, sizeof(pDummyPayload));

        if(res == kTTNSuccessfulTransmission){
            printf("msg sent.\n");
            ssd1306_display_clear();
            const char *pcTask1 = "msg sent.\n";
	        xTaskCreate(&task_ssd1306_display_text, "task_ssd1306_display_text", 4096, (void *) pcTask1, 5, NULL);
        }else{
            printf("Transm failed.\n");
            ssd1306_display_clear();
            const char *pcTask1 = "Transm failed.\n";
	        xTaskCreate(&task_ssd1306_display_text, "task_ssd1306_display_text", 4096, (void *) pcTask1, 5, NULL);
        }

        printf("sleeping...\n");
        ssd1306_display_clear();
        const char *pcTask2 = "sleeping...\n";
	    xTaskCreate(&task_ssd1306_display_text, "task_ssd1306_display_text", 4096, (void *) pcTask2, 5, NULL);
        vTaskDelay(TX_INTERVAL * pdMS_TO_TICKS(1000));
    }
}

void messageReceived(const uint8_t* message, size_t length, ttn_port_t port)
{
    printf("Message of %d bytes received on port %d:", length, port);
    for (int i = 0; i < length; i++)
        printf(" %02x", message[i]);
    printf("\n");
}

void dummyPayload(){

    uint16_t u16Temperature = (uint16_t) esp_random();
    uint16_t u16Pressure = (uint16_t) esp_random();
    uint16_t u16Windspeed = (uint16_t) esp_random();
    uint16_t u16Winddirection = (uint16_t) esp_random();

    if((esp_random()%2) == 0){
        pDummyPayload.auxState = AUX_STATE_DOOR_CLOSED;
    }else{
        pDummyPayload.auxState = AUX_STATE_DOOR_OPEN;
    }

    memcpy(pDummyPayload.degreesC, &u16Temperature, 2);
    memcpy(pDummyPayload.pressure, &u16Pressure, 2);
    memcpy(pDummyPayload.windspeed, &u16Windspeed, 2);
    memcpy(pDummyPayload.winddirection, &u16Winddirection, 2);



}

extern "C" void app_main(void)
{
    esp_err_t err;
    // Initialize the GPIO ISR handler service
    err = gpio_install_isr_service(ESP_INTR_FLAG_IRAM);
    ESP_ERROR_CHECK(err);
    
    // Initialize the NVS (non-volatile storage) for saving and restoring the keys
    err = nvs_flash_init();
    ESP_ERROR_CHECK(err);

    // Initialize SPI bus
    spi_bus_config_t spi_bus_config;
    spi_bus_config.miso_io_num = TTN_PIN_SPI_MISO;
    spi_bus_config.mosi_io_num = TTN_PIN_SPI_MOSI;
    spi_bus_config.sclk_io_num = TTN_PIN_SPI_SCLK;
    spi_bus_config.quadwp_io_num = -1;
    spi_bus_config.quadhd_io_num = -1;
    spi_bus_config.max_transfer_sz = 0;
    err = spi_bus_initialize(TTN_SPI_HOST, &spi_bus_config, TTN_SPI_DMA_CHAN);
    ESP_ERROR_CHECK(err);

    // Configure the SX127x pins
    ttn.configurePins(TTN_SPI_HOST, TTN_PIN_NSS, TTN_PIN_RXTX, TTN_PIN_RST, TTN_PIN_DIO0, TTN_PIN_DIO1);

    // The below line can be commented after the first run as the data is saved in NVS
    ttn.provision(CONFIG_DEVEUI, CONFIG_APPEUI, CONFIG_APPKEY);

    // Register callback for received messages
    ttn.onMessage(messageReceived);

    printf("random: %li\n", esp_random());

    i2c_master_init();
	ssd1306_init();

    printf("Joining...\n");
    ssd1306_display_clear();
    const char *pcTask1 = "Joining...\n";
	xTaskCreate(&task_ssd1306_display_text, "task_ssd1306_display_text", 4096, (void *) pcTask1, 5, NULL);
    if (ttn.join())
    {
        printf("Joined.\n");
        ssd1306_display_clear();
        const char *pcTask1 = "Joined.\n";
	    xTaskCreate(&task_ssd1306_display_text, "task_ssd1306_display_text", 4096, (void *) pcTask1, 5, NULL);
        xTaskCreate(sendMessages, "send_messages", 1024 * 4, (void* )0, 3, nullptr);
    }
    else
    {
        printf("Join failed. Goodbye\n");
        esp_restart();
    }
}
