#ifndef MAIN_H
#define MAIN_H

#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "driver/i2c.h"
#include "esp_random.h"
#include <cstring>

#include "TheThingsNetwork.h"

#define SDA_PIN GPIO_NUM_4
#define SCL_PIN GPIO_NUM_15

// Pins and other resources
#define TTN_SPI_HOST      HSPI_HOST
#define TTN_SPI_DMA_CHAN  1
#define TTN_PIN_SPI_SCLK  5
#define TTN_PIN_SPI_MOSI  27
#define TTN_PIN_SPI_MISO  19
#define TTN_PIN_NSS       18
#define TTN_PIN_RXTX      TTN_NOT_CONNECTED
#define TTN_PIN_RST       14
#define TTN_PIN_DIO0      26
#define TTN_PIN_DIO1      35

static TheThingsNetwork ttn;

const unsigned TX_INTERVAL = 60*15;
//static uint8_t msgData[] = "BBBBBBBB"; //8x 0x

#define AUX_STATE_DOOR_OPEN 0x1
#define AUX_STATE_DOOR_CLOSED 0x2

struct payload {
    uint8_t degreesC[2];
    uint8_t pressure[2];
    uint8_t windspeed[2];
    uint8_t winddirection[2];
    uint8_t auxState;
};


static struct payload pDummyPayload;


void dummyPayload(void);

#endif /* MAIN_H */